
  const moveDisc = (origin,destiny) => {
      
      const originStack = document.getElementById(origin)
      const destinyStack = document.getElementById(destiny)
      
      if (originStack.childElementCount > 0){
        const lastDiscOrigin = originStack.lastElementChild
          if ( destinyStack.childElementCount === 0){
            destinyStack.appendChild(lastDiscOrigin)
            lastDiscOrigin.classList.remove('selectedDisc')
            lastDiscOrigin.classList.add('unchecked')
          }
          if ( destinyStack.childElementCount > 0 && originStack.childElementCount > 0){
            
            const widthLastDiscDestiny = destinyStack.lastElementChild.clientWidth
            const widthLastDiscOrigin = originStack.lastElementChild.clientWidth
            if (widthLastDiscDestiny > widthLastDiscOrigin){
              const secondLastChildren = destinyStack.children[destinyStack.children.length -1]
              
              destinyStack.appendChild(lastDiscOrigin)
              lastDiscOrigin.classList.remove('selectedDisc')
              lastDiscOrigin.classList.add('unchecked')
              secondLastChildren.classList.remove('selectedDisc')
              secondLastChildren.classList.add('unchecked')
            }
          }
      }
      else{
        console.log('Não há discos para mover')
      }
  }
  
  const resetGame = (start,result) =>{
    const btn = document.getElementById('play-again')
    const discs = document.querySelectorAll('.disco')
    btn.addEventListener('click', () => {
      if(start.childElementCount < 5){
        document.getElementById('start').appendChild(discs[0])
        document.getElementById('start').appendChild(discs[1])
        document.getElementById('start').appendChild(discs[2])
        document.getElementById('start').appendChild(discs[3])
        document.getElementById('start').appendChild(discs[4])
      }
      result.textContent = ""
    }, false)
  }



function handleClick() {
    let selectedDisc = 'selectedDisc'
    let uncheckedDisc = 'unchecked'
    let selectStack = []
    
    const result = document.getElementById('result')

    const selectStackStart = document.getElementById('start');
    selectStackStart.addEventListener('click', () => {
       
      if(selectStackStart.lastElementChild !== null){
        const allClass = selectStackStart.lastElementChild.classList
          if (allClass.contains(uncheckedDisc)){
            allClass.add(selectedDisc)
            allClass.remove(uncheckedDisc)
              
              
              
          } else {
            allClass.add(uncheckedDisc) 
            allClass.remove(selectedDisc)
            }
      }      
            
          selectStack.push(event.currentTarget.id)
            if( selectStack.length === 3){
              
              selectStack = []
              selectStack.push(event.currentTarget.id)
            }
            

            if(selectStack.length === 2){
              moveDisc(selectStack[0],'start')
              
              
            }
      
    }, false);
    
    const selectStackOffset = document.getElementById('offset');
    selectStackOffset.addEventListener('click', () => { 
      
      if (selectStackOffset.lastElementChild !== null){
        const allClass = selectStackOffset.lastElementChild.classList
        if (allClass.contains(uncheckedDisc)){
          allClass.add(selectedDisc)
          allClass.remove(uncheckedDisc)
              
          } else {
              allClass.add(uncheckedDisc)
              allClass.remove(selectedDisc)
            }
      }       
        selectStack.push(event.currentTarget.id)
          

        if( selectStack.length === 3){
            
            selectStack = []
            selectStack.push(event.currentTarget.id)
          }
          

          if(selectStack.length === 2){
            
            moveDisc(selectStack[0],'offset')
            
          }

    }, false);

    const selectStackEnd = document.getElementById('end');
    selectStackEnd.addEventListener('click', () => { 
       
      if ( selectStackEnd.lastElementChild !== null){
        const allClass = selectStackEnd.lastElementChild.classList
        if (allClass.contains(uncheckedDisc)){
            allClass.add(selectedDisc)
            allClass.remove(uncheckedDisc)
        }else {
            allClass.add(uncheckedDisc)
            allClass.remove(selectedDisc)
          }
      }  
        selectStack.push(event.currentTarget.id)
          if( selectStack.length === 3){
            selectStack = []
            selectStack.push(event.currentTarget.id)
          }
          
          if(selectStack.length === 2){
            moveDisc(selectStack[0],'end')
          }
          endStackCount = selectStackEnd.childElementCount
          
          if (endStackCount === 5){
            
            setTimeout(result.textContent = 'Fim de Jogo, Você conseguiu!!!!!',1500)
            
          }
          
    }, false);

    
    resetGame(selectStackStart,result)

    
}
handleClick();






